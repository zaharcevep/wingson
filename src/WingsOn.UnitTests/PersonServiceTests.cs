using Moq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.BusinessLogic;
using WingsOn.Dal;
using WingsOn.Domain;
using Xunit;

namespace WingsOn.UnitTests
{
    public class PersonServiceTests
    {
        private readonly CultureInfo _cultureInfo = new CultureInfo("nl-NL");

        [Fact]
        public async Task GetAll_ReturnNotNullEqualToRepositoryItems()
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            personRepositoryMock.Setup(ps => ps.GetAll())
                                .Returns(Persons);


            var personService = new PersonService(personRepositoryMock.Object);

            #endregion

            #region Act

            var actual = await personService.GetAllAsync();

            #endregion

            #region Assert

            Assert.NotNull(actual);
            Assert.Equal(Persons.Count, actual.Count());

            var i = 0;
            foreach(var person in actual)
            {
                Assert.Equal(Persons[i++].Id, person.Id);
            }

            #endregion
        }

        [Theory]
        [InlineData(1)]
        [InlineData(100500)]
        [InlineData(int.MaxValue)]
        public async Task Get_NullForNotExistingId(int id)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            personRepositoryMock.Setup(ps => ps.Get(id))
                                .Returns<Person>(null);
            var personService = new PersonService(personRepositoryMock.Object);

            #endregion

            #region Act

            var actualPerson = await personService.GetByIdAsync(id);

            #endregion

            #region Assert

            Assert.Null(actualPerson);

            #endregion
        }

        [Theory]
        [InlineData(0, 91)]
        [InlineData(1, 69)]
        [InlineData(2, 77)]
        public async Task Get_CanFindPersonById(int index, int id)
        {
            #region Arrange

            var expected = Persons[index];
            var personRepositoryMock = new Mock<IRepository<Person>>();
            personRepositoryMock.Setup(ps => ps.Get(id))
                                .Returns(expected);
            var personService = new PersonService(personRepositoryMock.Object);

            #endregion

            #region Act

            var actual = await personService.GetByIdAsync(id);

            #endregion

            #region Assert

            Assert.True(expected.Equals(actual));

            #endregion
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(int.MinValue)]
        public async Task Get_NegativeOrZeroIdThrowArgumentOutOfRangeException(int id)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            personRepositoryMock.Setup(ps => ps.Get(id))
                                .Throws(new ArgumentOutOfRangeException());
            var personService = new PersonService(personRepositoryMock.Object);

            #endregion

            #region Act && Assert

            await Assert.ThrowsAnyAsync<ArgumentOutOfRangeException>(() => personService.GetByIdAsync(id));

            #endregion
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(int.MinValue)]
        public async Task Update_NegativeOrZeroIdThrowArgumentOutOfRangeException(int id)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            var personService = new PersonService(personRepositoryMock.Object);

            #endregion

            #region Act && Assert

            await Assert.ThrowsAnyAsync<ArgumentOutOfRangeException>(() => personService.UpdateAsync(id, string.Empty));

            #endregion
        }

        [Theory]
        [InlineData(91)]
        [InlineData(69)]
        [InlineData(77)]
        public async Task Update_EmptyEmailThrowArgumentNullException(int id)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            var personService = new PersonService(personRepositoryMock.Object);

            #endregion

            #region Act && Assert

            await Assert.ThrowsAnyAsync<ArgumentNullException>(() => personService.UpdateAsync(id, string.Empty));

            #endregion
        }

        [Theory]
        [InlineData("asdf")]
        [InlineData("as df")]
        [InlineData("@domain.com")]
        [InlineData("email_without_domain_name@.com")]
        public async Task Update_InvalidEmailFormatThrowArgumentNullException(string email)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            var personService = new PersonService(personRepositoryMock.Object);

            #endregion

            #region Act && Assert

            await Assert.ThrowsAnyAsync<ArgumentException>(() => personService.UpdateAsync(91, email));

            #endregion
        }

        [Theory]
        [InlineData(69, "egestas.a.dui@aliquet.ca")]
        [InlineData(91, "non.cursus.non@turpisIncondimentum.co.uk")]
        [InlineData(77, "non.cursus.non@turpisIncondimentum.co.uk")]
        public async Task Update_DuplicateEmailThrowArgumentException(int id, string email)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            personRepositoryMock.Setup(pr => pr.GetAll())
                                .Returns(Persons);
            var personService = new PersonService(personRepositoryMock.Object);

            #endregion

            #region Act && Assert

            await Assert.ThrowsAnyAsync<ArgumentException>(() => personService.UpdateAsync(id, email));

            #endregion
        }

        [Theory]
        [InlineData(69, "new_one_egestas.a.dui@aliquet.ca")]
        [InlineData(91, "new_one_non.cursus.non@turpisIncondimentum.co.uk")]
        [InlineData(77, "new_one_non.cursus.non@turpisIncondimentum.co.uk")]
        public async Task Update_FailedRepositorySaveCantUpdate(int id, string email)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            personRepositoryMock.Setup(ps => ps.Get(It.IsAny<int>()))
                                .Returns(Persons[0]);
            personRepositoryMock.Setup(pr => pr.Save(It.IsAny<Person>()))
                                .Throws(new Exception());
            var personService = new PersonService(personRepositoryMock.Object);

            #endregion

            #region Act

            var actual = await personService.UpdateAsync(id, email);

            #endregion

            #region Assert

            Assert.False(actual);

            #endregion
        }

        [Theory]
        [InlineData(69, "new_one_egestas.a.dui@aliquet.ca")]
        [InlineData(91, "new_one_non.cursus.non@turpisIncondimentum.co.uk")]
        [InlineData(77, "new_one_non.cursus.non@turpisIncondimentum.co.uk")]
        public async Task Update_CanUpdate(int id, string email)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            personRepositoryMock.Setup(ps => ps.Get(It.IsAny<int>()))
                                .Returns(Persons[0]);
            personRepositoryMock.Setup(pr => pr.Save(It.IsAny<Person>()))
                                .Verifiable();
            var personService = new PersonService(personRepositoryMock.Object);

            #endregion

            #region Act

            var actual = await personService.UpdateAsync(id, email);

            #endregion

            #region Assert

            personRepositoryMock.Verify(pr => pr.Save(It.IsAny<Person>()), Times.Exactly(1));
            Assert.True(actual);

            #endregion
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("    ")]
        [InlineData("\t")]
        public async Task Create_EmptyEmailThrowArgumentNullException(string email)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            var personService = new PersonService(personRepositoryMock.Object);

            var newPerson = new Person { Id = 0, Email = email };

            #endregion

            #region Act && Assert

            await Assert.ThrowsAnyAsync<ArgumentNullException>(() => personService.CreateAsync(newPerson));

            #endregion
        }

        [Theory]
        [InlineData("asdf")]
        [InlineData("as df")]
        [InlineData("@domain.com")]
        [InlineData("email_without_domain_name@.com")]
        public async Task Create_InvalidEmailFormatThrowArgumentNullException(string email)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            var personService = new PersonService(personRepositoryMock.Object);

            var newPerson = new Person { Id = 777, Email = email };

            #endregion

            #region Act && Assert

            await Assert.ThrowsAnyAsync<ArgumentException>(() => personService.CreateAsync(newPerson));

            #endregion
        }

        [Theory]
        [InlineData("egestas.a.dui@aliquet.ca")]
        [InlineData("non.cursus.non@turpisIncondimentum.co.uk")]
        [InlineData("egestas.lacinia@Proinmi.com")]
        public async Task Create_DuplicateEmailThrowArgumentException(string email)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            personRepositoryMock.Setup(pr => pr.GetAll())
                                .Returns(Persons);
            personRepositoryMock.Setup(pr => pr.Save(It.IsAny<Person>()))
                                .Verifiable();
            var personService = new PersonService(personRepositoryMock.Object);

            var newPerson = new Person { Id = 777, Email = email };

            #endregion

            #region Act && Assert

            await Assert.ThrowsAsync<ArgumentException>(() => personService.CreateAsync(newPerson));

            #endregion
        }

        [Theory]
        [InlineData("new_one_egestas.a.dui@aliquet.ca")]
        [InlineData("new_one_non.cursus.non@turpisIncondimentum.co.uk")]
        [InlineData("egestas.lacinia@Proinmi.com")]
        public async Task Create_FailedRepositorySaveCantUpdate(string email)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            personRepositoryMock.Setup(ps => ps.Get(It.IsAny<int>()))
                                .Returns(Persons[0]);
            personRepositoryMock.Setup(pr => pr.Save(It.IsAny<Person>()))
                                .Throws(new Exception());
            var personService = new PersonService(personRepositoryMock.Object);

            var newPerson = new Person { Id = 0, Email = email };

            #endregion

            #region Act

            var actual = await personService.CreateAsync(newPerson);

            #endregion

            #region Assert

            Assert.False(actual);

            #endregion
        }

        [Theory]
        [InlineData("new_one_egestas.a.dui@aliquet.ca")]
        [InlineData("new_one_non.cursus.non@turpisIncondimentum.co.uk")]
        public async Task Create_CanCreate(string email)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            personRepositoryMock.Setup(ps => ps.GetAll())
                                .Returns(Persons);
            personRepositoryMock.Setup(pr => pr.Save(It.IsAny<Person>()))
                                .Verifiable();
            var personService = new PersonService(personRepositoryMock.Object);

            var newPerson = new Person { Email = email };

            #endregion

            #region Act

            var actual = await personService.CreateAsync(newPerson);

            #endregion

            #region Assert

            personRepositoryMock.Verify(pr => pr.Save(It.IsAny<Person>()), Times.Exactly(1));
            Assert.True(actual);

            #endregion
        }

        [Theory]
        [InlineData(int.MaxValue)]
        [InlineData(1)]
        [InlineData(100500)]
        public async Task Create_PositiveIdThrowArgumentException(int id)
        {
            #region Arrange

            var personRepositoryMock = new Mock<IRepository<Person>>();
            var personService = new PersonService(personRepositoryMock.Object);

            var newPerson = new Person { Id = id, Email = "test@domain.cpom" };

            #endregion

            #region Act && Assert

            await Assert.ThrowsAnyAsync<ArgumentException>(() => personService.CreateAsync(newPerson));

            #endregion
        }

        private IReadOnlyList<Person> Persons => new[]
        {
            new Person
                {
                    Id = 91,
                    Address = "805-1408 Mi Rd.",
                    DateBirth = DateTime.Parse("24/09/1980", _cultureInfo),
                    Email = "egestas.a.dui@aliquet.ca",
                    Gender = GenderType.Male,
                    Name = "Kendall Velazquez"
                },
                new Person
                {
                    Id = 69,
                    Address = "P.O. Box 344, 5822 Curabitur Rd.",
                    DateBirth = DateTime.Parse("27/11/1948", _cultureInfo),
                    Email = "non.cursus.non@turpisIncondimentum.co.uk",
                    Gender = GenderType.Female,
                    Name = "Claire Stephens"
                },
                new Person
                {
                    Id = 77,
                    Address = "P.O. Box 795, 1956 Odio. Rd.",
                    DateBirth = DateTime.Parse("01/01/1940", _cultureInfo),
                    Email = "egestas.lacinia@Proinmi.com",
                    Gender = GenderType.Male,
                    Name = "Branden Johnston"
                }
        };
    }
}
