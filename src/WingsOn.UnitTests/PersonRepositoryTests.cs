using System;
using System.Linq;
using WingsOn.Dal;
using Xunit;

namespace WingsOn.UnitTests
{
    public class PersonRepositoryTests
    {
        [Fact]
        public void GetAll_ReturnNotNull()
        {
            #region Arrange

            var personRepository = new PersonRepository();

            #endregion

            #region Act

            var actual = personRepository.GetAll();

            #endregion

            #region Assert

            Assert.NotNull(actual);

            #endregion
        }

        [Fact]
        public void Get_ReturnNullForNegativeIdElement()
        {
            #region Arrange

            var personRepository = new PersonRepository();

            #endregion

            #region Act

            var actual = personRepository.Get(-1);

            #endregion

            #region Assert

            Assert.Null(actual);

            #endregion
        }

        [Fact]
        public void Get_CanReturnSingleElement()
        {
            #region Arrange

            var personRepository = new PersonRepository();
            var allPersons = personRepository.GetAll();

            var first = allPersons.First();

            #endregion

            #region Act

            var actual = personRepository.Get(first.Id);

            #endregion

            #region Assert

            Assert.True(first.Equals(actual));

            #endregion
        }

        // Workaround test
        [Fact]
        public void Get_CantReturnDuplicatesById()
        {
            #region Arrange

            var personRepository = new PersonRepository();
            var allPersons = personRepository.GetAll();

            var first = allPersons.First();
            var last = allPersons.Last();
            last.Id = first.Id;

            #endregion

            #region Act && Assert
            
            Assert.Throws<InvalidOperationException>(() => personRepository.Get(first.Id));

            #endregion
        }
    }
}
