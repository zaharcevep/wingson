using Moq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.BusinessLogic;
using WingsOn.Dal;
using WingsOn.Domain;
using Xunit;

namespace WingsOn.UnitTests
{
    public class BookingServiceTests
    {
        private readonly CultureInfo _cultureInfo = new CultureInfo("nl-NL");

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("    ")]
        [InlineData("\t")]
        public async Task GetPassengersByFlightAsync_NullAndEmptyFlightThrowArgumentNullException(string flightNumber)
        {
            #region Arrange

            var bookingRepositoryMock = new Mock<IRepository<Booking>>();

            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act && Assert

            await Assert.ThrowsAsync<ArgumentNullException>(() => bookingService.GetPassengersByFlightAsync(flightNumber));

            #endregion
        }

        [Fact]
        public async Task GetPassengersByFlightAsync_CantFindNotExistingFlight()
        {
            #region Arrange

            var expected = new List<Person>();
            var bookingRepositoryMock = new Mock<IRepository<Booking>>();
            bookingRepositoryMock.Setup(ps => ps.GetAll())
                                 .Returns(Bookings);


            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act

            var actual = await bookingService.GetPassengersByFlightAsync("FakeFlight");

            #endregion

            #region Assert

            Assert.Equal(expected.Count, actual.Count());

            #endregion
        }

        [Theory]
        [InlineData("BB124")]
        [InlineData("PZ696")]
        public async Task GetPassengersByFlightAsync_HaveToFindExistingFlight(string flightNumber)
        {
            #region Arrange

            var bookingRepositoryMock = new Mock<IRepository<Booking>>();
            bookingRepositoryMock.Setup(ps => ps.GetAll())
                                 .Returns(Bookings);


            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act

            var actual = await bookingService.GetPassengersByFlightAsync(flightNumber);

            #endregion

            #region Assert

            Assert.True(actual.Count() > 0);

            #endregion
        }

        [Theory]
        [InlineData("Bb124")]
        [InlineData("pZ696")]
        public async Task GetPassengersByFlightAsync_HaveToFindExistingFlightInDifferentCases(string flightNumber)
        {
            #region Arrange

            var bookingRepositoryMock = new Mock<IRepository<Booking>>();
            bookingRepositoryMock.Setup(ps => ps.GetAll())
                                 .Returns(Bookings);


            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act

            var actual = await bookingService.GetPassengersByFlightAsync(flightNumber);

            #endregion

            #region Assert

            Assert.True(actual.Count() > 0);

            #endregion
        }

        [Fact]
        public async Task GetPassengersByFlightAsync_UniqueFlightReturnFullListOfPassangers()
        {
            #region Arrange

            var expected = Bookings.Last().Passengers.ToArray();
            var bookingRepositoryMock = new Mock<IRepository<Booking>>();
            bookingRepositoryMock.Setup(ps => ps.GetAll())
                                 .Returns(Bookings);

            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act

            var actual = await bookingService.GetPassengersByFlightAsync("BB124");

            #endregion

            #region Assert

            Assert.NotNull(actual);
            Assert.Equal(expected.Count(), actual.Count());

            var i = 0;
            foreach (var passanger in actual)
            {
                Assert.Equal(expected[i++].Name, passanger.Name);
            }

            #endregion
        }

        [Fact]
        public async Task GetPassengersByFlightAsync_DuplicateFlightReturnDistinctListOfPassangers()
        {
            #region Arrange

            var expected = new List<Person>(Bookings[0].Passengers);
            expected.AddRange(Bookings[1].Passengers);  // with the same flightNumber as previouse
            expected.RemoveAt(4); // remove duplicate manualy

            var bookingRepositoryMock = new Mock<IRepository<Booking>>();
            bookingRepositoryMock.Setup(ps => ps.GetAll())
                                 .Returns(Bookings);

            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act

            var actual = await bookingService.GetPassengersByFlightAsync("PZ696");

            #endregion

            #region Assert

            Assert.NotNull(actual);
            Assert.Equal(expected.Count(), actual.Count());

            var i = 0;
            foreach (var passanger in actual)
            {
                Assert.Equal(expected[i++].Id, passanger.Id);
            }

            #endregion
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("    ")]
        [InlineData("\t")]
        public async Task GetPassengersByGenderAsync_NullAndEmptyGenderThrowArgumentNullException(string gender)
        {
            #region Arrange

            var bookingRepositoryMock = new Mock<IRepository<Booking>>();

            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act && Assert

            await Assert.ThrowsAsync<ArgumentNullException>(() => bookingService.GetPassengersByGenderAsync(gender));

            #endregion
        }

        [Theory]
        [InlineData("test")]
        [InlineData("don't know")]
        [InlineData("Transgender")]
        public async Task GetPassengersByGenderAsync_NotExistingGenderThrowNotSupportedException(string gender)
        {
            #region Arrange

            var expected = new List<Person>();
            var bookingRepositoryMock = new Mock<IRepository<Booking>>();
            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act && Assert

            await Assert.ThrowsAsync<NotSupportedException>(() => bookingService.GetPassengersByGenderAsync(gender));

            #endregion
        }

        [Theory]
        [InlineData("m", 4)]
        [InlineData("male", 4)]
        [InlineData("M", 4)]
        [InlineData("Male", 4)]
        [InlineData("f", 3)]
        [InlineData("female", 3)]
        [InlineData("F", 3)]
        [InlineData("FeMale", 3)]
        public async Task GetPassengersByGenderAsync_HaveToFindPassengersInDifferentCasesOfGender(string gender, int cnt)
        {
            #region Arrange

            var bookingRepositoryMock = new Mock<IRepository<Booking>>();
            bookingRepositoryMock.Setup(ps => ps.GetAll())
                                 .Returns(Bookings);


            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act

            var actual = await bookingService.GetPassengersByGenderAsync(gender);

            #endregion

            #region Assert

            Assert.True(actual.Count() > 0);
            Assert.Equal(cnt, actual.Count());

            #endregion
        }

        [Fact]
        public async Task GetPassengersByGenderAsync_PassangerListForParticularGenderWithoutDuplicates()
        {
            #region Arrange

            var bookingRepositoryMock = new Mock<IRepository<Booking>>();
            bookingRepositoryMock.Setup(ps => ps.GetAll())
                                 .Returns(Bookings);

            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act

            var actual = await bookingService.GetPassengersByGenderAsync("Female");

            #endregion

            #region Assert

            Assert.NotNull(actual);
            Assert.Equal(3, actual.Count());

            #endregion
        }

        [Fact]
        public async Task GetPassengersByGenderAsync_AllPassangersHaveParticularGenderType()
        {
            #region Arrange

            var bookingRepositoryMock = new Mock<IRepository<Booking>>();
            bookingRepositoryMock.Setup(ps => ps.GetAll())
                                 .Returns(Bookings);

            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act

            var actual = await bookingService.GetPassengersByGenderAsync("f");

            #endregion

            #region Assert

            Assert.NotNull(actual);

            foreach (var passanger in actual)
            {
                Assert.Equal(GenderType.Female, passanger.Gender);
            }

            #endregion
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("    ")]
        [InlineData("\t")]
        public async Task CreateBookingAsync_NullAndEmptyFlightThrowArgumentNullException(string flight)
        {
            #region Arrange

            var bookingRepositoryMock = new Mock<IRepository<Booking>>();

            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act && Assert

            await Assert.ThrowsAsync<ArgumentNullException>(() => bookingService.CreateBookingAsync(flight, null));

            #endregion
        }

        [Fact]
        public async Task CreateBookingAsync_NullPassengersThrowArgumentNullException()
        {
            #region Arrange

            var flight = "PZ696";
            var bookingRepositoryMock = new Mock<IRepository<Booking>>();

            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act && Assert

            await Assert.ThrowsAsync<ArgumentNullException>(() => bookingService.CreateBookingAsync(flight, null));

            #endregion
        }

        [Fact]
        public async Task CreateBookingAsync_EmptyPassengersThrowArgumentException()
        {
            #region Arrange

            var flight = "PZ696";
            var bookingRepositoryMock = new Mock<IRepository<Booking>>();

            var bookingService = new BookingService(bookingRepositoryMock.Object, null, null, null, null);

            #endregion

            #region Act && Assert

            await Assert.ThrowsAsync<ArgumentException>(() => bookingService.CreateBookingAsync(flight, new List<Person>()));

            #endregion
        }

        [Theory]
        [InlineData("PZ69644")]
        [InlineData("BB124123")]
        public async Task CreateBookingAsync_NotExistingFlightThrowArgumentException(string flight)
        {
            #region Arrange

            var bookingRepositoryMock = new Mock<IRepository<Booking>>();
            var flightRepositoryMock = new Mock<IRepository<Flight>>();
            flightRepositoryMock.Setup(fr => fr.GetAll())
                                .Returns(Flights);
            var passengers = new List<Person> { new Person() };

            var bookingService = new BookingService(bookingRepositoryMock.Object, flightRepositoryMock.Object, null, null, null);

            #endregion

            #region Act && Assert

            await Assert.ThrowsAsync<ArgumentException>(() => bookingService.CreateBookingAsync(flight, passengers));

            #endregion
        }

        [Theory]
        [InlineData("PZ696")]
        [InlineData("BB124")]
        public async Task CreateBookingAsync_PersonWithEmptOrNotExistingYetIdWillCreated(string flight)
        {
            #region Arrange

            var bookingRepositoryMock = new Mock<IRepository<Booking>>();
            var flightRepositoryMock = new Mock<IRepository<Flight>>();
            flightRepositoryMock.Setup(fr => fr.GetAll())
                                .Returns(Flights);
            var personRepositoryMock = new Mock<IRepository<Person>>();
            personRepositoryMock.Setup(ps => ps.Get(It.IsAny<int>()))
                                .Returns<Person>(null);
            var personServiceMock = new Mock<IPersonService>();
            personServiceMock.Setup(ps => ps.CreateAsync(It.IsAny<Person>()))
                             .Verifiable();
            var passengers = new List<Person> { new Person { Email = "test@mail.com"} };

            var bookingService = new BookingService(bookingRepositoryMock.Object,
                                                    flightRepositoryMock.Object,
                                                    personRepositoryMock.Object,
                                                    personServiceMock.Object,
                                                    null);

            #endregion

            #region Act

            await bookingService.CreateBookingAsync(flight, passengers);

            #endregion

            #region Assert

            personServiceMock.Verify(ps => ps.CreateAsync(It.IsAny<Person>()), Times.Exactly(1));

            #endregion
        }

        private IReadOnlyList<Person> Persons => new[]
        {
            new Person
                {
                    Id = 91,
                    Address = "805-1408 Mi Rd.",
                    DateBirth = DateTime.Parse("24/09/1980", _cultureInfo),
                    Email = "egestas.a.dui@aliquet.ca",
                    Gender = GenderType.Male,
                    Name = "Kendall Velazquez"
                },
                new Person
                {
                    Id = 69,
                    Address = "P.O. Box 344, 5822 Curabitur Rd.",
                    DateBirth = DateTime.Parse("27/11/1948", _cultureInfo),
                    Email = "non.cursus.non@turpisIncondimentum.co.uk",
                    Gender = GenderType.Female,
                    Name = "Claire Stephens"
                }
        };

        private IReadOnlyList<Flight> Flights => new[]
        {
            new Flight
                {
                    Id = 30,
                    Number = "BB124",
                    DepartureDate = DateTime.Parse("12/02/2012 16:50", _cultureInfo),
                    ArrivalDate = DateTime.Parse("13/02/2012 00:00", _cultureInfo),
                    Price = 196.1m
                },
                new Flight
                {
                    Id = 81,
                    Number = "PZ696",
                    DepartureDate = DateTime.Parse("20/02/2000 17:50", _cultureInfo),
                    ArrivalDate = DateTime.Parse("20/02/2000 19:00", _cultureInfo),
                    Price = 95.2m
                }
        };

        private IReadOnlyList<Booking> Bookings => new[]
        {
            new Booking
                {
                    Id = 83,
                    Number = "WO-151277",
                    Customer = new Person { Id = 15 },
                    DateBooking = DateTime.Parse("12/02/2000 12:55", _cultureInfo),
                    Flight = new Flight { Number = "PZ696" },
                    Passengers = new []
                    {
                        new Person { Id = 15, Gender = GenderType.Male },
                        new Person { Id = 16, Gender = GenderType.Female },
                        new Person { Id = 17, Gender = GenderType.Male }
                    }
                },
                new Booking
                {
                    Id = 34,
                    Number = "WO-694142",
                    Customer = new Person { Id = 25 },
                    DateBooking = DateTime.Parse("13/02/2000 16:37", _cultureInfo),
                    Flight = new Flight { Number = "PZ696" },
                    Passengers = new []
                    {
                        new Person { Id = 25, Gender = GenderType.Female },
                        new Person { Id = 16, Gender = GenderType.Female },  // duplicate for previouse one
                        new Person { Id = 26, Gender = GenderType.Female }
                    }
                },
                new Booking
                {
                    Id = 90,
                    Number = "WO-139716",
                    Customer = new Person { Id = 777 },
                    DateBooking = DateTime.Parse("03/12/2011 16:50", _cultureInfo),
                    Flight = new Flight { Number = "BB124" },
                    Passengers = new []
                    {
                        new Person { Id = 777, Gender = GenderType.Male },
                        new Person { Id = 77, Gender = GenderType.Male }
                    }
                }
        };
    }
}