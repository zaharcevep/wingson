﻿using System;
using WingsOn.Domain;

namespace WingsOn.BusinessLogic.Tools
{
    internal static class Parsers
    {
        public static GenderType GetGender(string gender)
        {
            gender = gender.ToLowerInvariant();

            if (gender == "m" || gender == "male")
            {
                return GenderType.Male;
            }
            else if(gender == "f" || gender == "female")
            {
                return GenderType.Female;
            }
            else
            {
                throw new NotSupportedException($"Gender {gender} does not supported yet");
            }
        }
    }
}
