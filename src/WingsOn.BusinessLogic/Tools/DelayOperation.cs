﻿using System.Threading.Tasks;

namespace WingsOn.BusinessLogic.Tools
{
    internal static class DelayOperation
    {
        public static async Task SimulateWorkAsync()
        {
            await Task.Delay(500);
        }
    }
}
