﻿using System;

namespace WingsOn.BusinessLogic
{
    public interface IDateService
    {
        DateTime GetNow();
        DateTime GetNowUtc();
    }
}
