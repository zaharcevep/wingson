﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WingsOn.Domain;

namespace WingsOn.BusinessLogic
{
    public interface IPersonService
    {
        Task<IEnumerable<Person>> GetAllAsync();
        Task<Person> GetByIdAsync(int id);
        Task<bool> UpdateAsync(int id, string newEmail);
        Task<bool> CreateAsync(Person person);
    }
}
