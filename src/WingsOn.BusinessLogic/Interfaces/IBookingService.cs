﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WingsOn.Domain;

namespace WingsOn.BusinessLogic
{
    public interface IBookingService
    {
        Task<IEnumerable<Person>> GetPassengersByFlightAsync(string flightNumber);
        Task<IEnumerable<Person>> GetPassengersByGenderAsync(string gender);
        Task<bool> CreateBookingAsync(string flight, IEnumerable<Person> passengers);
    }
}
