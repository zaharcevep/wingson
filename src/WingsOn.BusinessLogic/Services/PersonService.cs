﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using WingsOn.BusinessLogic.Tools;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.BusinessLogic
{
    public class PersonService : IPersonService
    {
        private readonly IRepository<Person> _repository;

        public PersonService(IRepository<Person> repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<Person>> GetAllAsync()
        {
            await DelayOperation.SimulateWorkAsync();

            return _repository.GetAll();
        }

        public async Task<Person> GetByIdAsync(int id)
        {
            await DelayOperation.SimulateWorkAsync();

            if (id <= 0)
                throw new ArgumentOutOfRangeException("id");

            return _repository.Get(id);
        }

        public async Task<bool> UpdateAsync(int id, string newEmail)
        {
            var result = true;
            await DelayOperation.SimulateWorkAsync();

            if (id <= 0)
                throw new ArgumentOutOfRangeException("Id");

            if (string.IsNullOrWhiteSpace(newEmail))
                throw new ArgumentNullException("Email");

            newEmail = newEmail.ToLowerInvariant();
            if (!CheckEmailFormat(newEmail))
                throw new ArgumentException($"Email \\{newEmail}\\ has wrong format", "Email");

            var updatingPerson = _repository.Get(id);
            if (updatingPerson == null)
                throw new ArgumentException($"There is no person with id \\{id}\\", "Id");

            if (updatingPerson.Email.ToLowerInvariant() == newEmail)
                return result;

            var theSameEmail = _repository.GetAll().FirstOrDefault(p => p.Email.ToLowerInvariant() == newEmail);

            if (theSameEmail != null)
                throw new ArgumentException($"Person with email \\{newEmail}\\ already exists in the system", "Email");

            updatingPerson.Email = newEmail;

            try
            {
                _repository.Save(updatingPerson);
            }
            catch (Exception)
            {
                // TODO log Exception
                result = false;
            }

            return result;
        }

        public async Task<bool> CreateAsync(Person person)
        {
            var result = true;
            await DelayOperation.SimulateWorkAsync();

            if (null == person)
                throw new ArgumentNullException("Person");

            if (person.Id > 0)
                throw new ArgumentException($"Person id \\{person.Id}\\ is not valid in creat scope", "Person.Id");

            if (string.IsNullOrWhiteSpace(person.Email))
                throw new ArgumentNullException("Person.Email");

            person.Email = person.Email.ToLowerInvariant();
            if (!CheckEmailFormat(person.Email))
                throw new ArgumentException($"Email \\{person.Email}\\ has wrong format", "Email");

            var theSameEmail = _repository.GetAll().FirstOrDefault(p => p.Email.ToLowerInvariant() == person.Email);
            if (null != theSameEmail)
                throw new ArgumentException($"Person with the same email \\{person.Email}\\ already exists", "Person.Email");

            try
            {
                var maxId = _repository.GetAll().Max(p => p.Id) + 1;

                person.Id = maxId;

                _repository.Save(person);
            }
            catch (Exception)
            {
                // TODO log it
                result = false;
            }

            return result;
        }

        private bool CheckEmailFormat(string email)
        {
            try
            {
                var m = new MailAddress(email);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
