﻿using System;

namespace WingsOn.BusinessLogic
{
    public class DateService : IDateService
    {
        public DateTime GetNow()
        {
            return DateTime.Now;
        }

        public DateTime GetNowUtc()
        {
            return DateTime.UtcNow;
        }
    }
}
