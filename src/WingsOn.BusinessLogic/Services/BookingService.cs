﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WingsOn.BusinessLogic.Tools;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.BusinessLogic
{
    public class BookingService : IBookingService
    {
        private readonly IRepository<Booking> _bookingRepository;
        private readonly IRepository<Flight> _flightRepository;
        private readonly IRepository<Person> _personRepository;
        private readonly IPersonService _personService;
        private readonly IDateService _dateService;

        public BookingService(IRepository<Booking> bookingRepository,
                              IRepository<Flight> flightRepository,
                              IRepository<Person> personRepository,
                              IPersonService personService,
                              IDateService dateService)
        {
            _bookingRepository = bookingRepository;
            _flightRepository = flightRepository;
            _personRepository = personRepository;
            _personService = personService;
            _dateService = dateService;
        }

        public async Task<IEnumerable<Person>> GetPassengersByFlightAsync(string flightNumber)
        {
            if (string.IsNullOrWhiteSpace(flightNumber))
                throw new ArgumentNullException("FlightNumber");

            await DelayOperation.SimulateWorkAsync();

            var res = _bookingRepository.GetAll()
                                        .Where(b => b.Flight.Number.ToLowerInvariant() == flightNumber.ToLowerInvariant())
                                        .SelectMany(b => b.Passengers)
                                        .GroupBy(p => p.Id).Select(g => g.First()) // simulate Distinct
                                        ;
            return res;
        }

        public async Task<IEnumerable<Person>> GetPassengersByGenderAsync(string gender)
        {
            if (string.IsNullOrWhiteSpace(gender))
                throw new ArgumentNullException("Gender");

            await DelayOperation.SimulateWorkAsync();

            var genderType = Parsers.GetGender(gender);

            var res = _bookingRepository.GetAll()
                                        .SelectMany(b => b.Passengers)
                                        .Where(p => p.Gender == genderType)
                                        .GroupBy(p => p.Id).Select(g => g.First()) // simulate Distinct
                                        ;
            return res;
        }

        public async Task<bool> CreateBookingAsync(string flightNumber, IEnumerable<Person> passengers)
        {
            if (string.IsNullOrWhiteSpace(flightNumber))
                throw new ArgumentNullException("FlightNumber");

            if (null == passengers)
                throw new ArgumentNullException("Passengers");

            if (!passengers.Any())
                throw new ArgumentException($"Empty list of passangers for flight \\{flightNumber}\\", "Passengers");

            var result = true;
            var flightInLower = flightNumber.ToLowerInvariant();
            var flight = _flightRepository.GetAll().SingleOrDefault(f => f.Number.ToLowerInvariant() == flightInLower);

            if (flight == null)
                throw new ArgumentException($"Incorrect flight number \\{flightNumber}\\", "FlightNumber");

            try
            {
                foreach (var passenger in passengers)
                {
                    var isNewPassenger = _personRepository.Get(passenger.Id) == null;
                    if (isNewPassenger)
                        await _personService.CreateAsync(passenger);
                }

                var maxId = _bookingRepository.GetAll().Max(b => b.Id) + 1;

                var creatingBooking = new Booking
                {
                    Customer = passengers.First(),          // TODO not clear convention to passengers first one is the Customer of booking
                    DateBooking = _dateService.GetNowUtc(), // TODO add timezone
                    Flight = flight,
                    Number = GenerateUniqueBookingNumber(),
                    Passengers = passengers,
                    Id = maxId
                };

                _bookingRepository.Save(creatingBooking);
            }
            catch (Exception)
            {
                // TODO log it
                result = false;
            }

            return result;
        }

        private string GenerateUniqueBookingNumber()
        {
            while (true)
            {
                var number = MagicBookingNumberGenerator();
                var theSameNumberBooking = _bookingRepository.GetAll().FirstOrDefault(b => b.Number.ToLowerInvariant() == number.ToLowerInvariant());
                if (null == theSameNumberBooking)
                    return number;
            }
        }

        private string MagicBookingNumberGenerator()
        {
            Random r = new Random();
            var rInt = r.Next(100000, 999999);
            return $"WO-{rInt}";
        }
    }
}
