﻿using Microsoft.Extensions.DependencyInjection;
using WingsOn.BusinessLogic;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Api.ServiceCollectionExtensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddBusinnesLogic(this IServiceCollection services)
        {
            services.AddScoped<IRepository<Person>, PersonRepository>();
            services.AddScoped<IRepository<Booking>, BookingRepository>();
            services.AddScoped<IRepository<Flight>, FlightRepository>();
            
            services.AddScoped<IDateService, DateService>();
            services.AddScoped<IPersonService, PersonService>();
            services.AddScoped<IBookingService, BookingService>();
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(Constants.App.Version, new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Version = Constants.App.Version,
                    Title = Constants.App.Name,
                    Description = Constants.App.Description
                });
            });
        }
    }
}
