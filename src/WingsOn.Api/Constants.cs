﻿namespace WingsOn.Api
{
    public static class Constants
    {
        public static class App
        {
            public const string Version = "v1";
            public const string Name = "WingsOn API";
            public const string Description = "ASP.NET Core Web API for WingsOn app";

            public const string ApiBaseRoute = "api/" + Version;
        }
    }
}
