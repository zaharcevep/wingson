﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WingsOn.BusinessLogic;
using WingsOn.Domain;

namespace WingsOn.Api.Controllers
{
    [Route(Constants.App.ApiBaseRoute + "/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        private readonly IBookingService _bookingService;

        public BookingController(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }

        [HttpGet("flight/{flight}")]
        public async Task<IActionResult> GetFlight(string flight)
        {
            try
            {
                var booking = await _bookingService.GetPassengersByFlightAsync(flight);

                if (!booking.Any())
                    return new NotFoundResult();

                return new OkObjectResult(booking);
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message });
            }
        }

        [HttpGet("passengers/{gender}")]
        public async Task<IActionResult> GetPassengers(string gender)
        {
            try
            {
                var passengers = await _bookingService.GetPassengersByGenderAsync(gender);

                if (!passengers.Any())
                    return new NotFoundResult();

                return new OkObjectResult(passengers);
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message });
            }
        }

        [HttpPost("create/{flight}")]
        public async Task<IActionResult> Create(string flight, [FromBody] Person passenger)
        {
            try
            {
                var res = await _bookingService.CreateBookingAsync(flight, new List<Person> { passenger });

                if (!res)
                    return BadRequest(new { message = "Booking haven't been created" });

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message });
            }
        }
    }
}
