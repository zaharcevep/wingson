﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WingsOn.BusinessLogic;

namespace WingsOn.Api.Controllers
{
    [Route(Constants.App.ApiBaseRoute + "/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly IPersonService _personService;

        public PersonController(IPersonService personService)
        {
            _personService = personService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var persons = await _personService.GetAllAsync();

                return new OkObjectResult(persons);
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message });
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var person = await _personService.GetByIdAsync(id);

                if (person == null)
                    return new NotFoundResult();

                return new OkObjectResult(person);
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message });
            }
        }

        [HttpPut("{id}/{email}")]
        public async Task<IActionResult> Put(int id, string email)
        {
            try
            {
                var isUpdated = await _personService.UpdateAsync(id, email);

                if (!isUpdated)
                    return BadRequest(new { message = "Unable to update email" });

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message });
            }
        }
    }
}
